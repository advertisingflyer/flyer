<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 04.09.2018
 * Time: 19:52
 */

use yii\widgets\ListView;

/* @var $this \yii\web\View */
/* @var $query string */

$this->title = $query;

?>
<div class="search-list">
    <?= ListView::widget([
        'dataProvider' => $resultCustomer,
        'emptyText' => false,
        'summary' => '',
        'options' => ['data-pjax'=> true],
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model) {
            return $this->renderAjax('_data_search_customer',[
                'model' => $model
            ]);
        },
    ]) ?>
</div>
<div class="search-list">
    <?= ListView::widget([
        'dataProvider' => $resultOrder,
        'emptyText' => false,
        'summary' => '',
        'options' => ['data-pjax'=> true],
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model) {
            return $this->renderAjax('_data_search_order',[
                'model' => $model
            ]);
        },
    ]) ?>
</div>