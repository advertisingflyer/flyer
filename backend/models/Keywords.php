<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "keywords".
 *
 * @property int $id
 * @property string $key
 * @property string $table
 * @property string $column
 * @property int $state
 */
class Keywords extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'keywords';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key', 'table', 'column', 'state'], 'required'],
            [['state'], 'integer'],
            [['key'], 'string', 'max' => 255],
            [['table', 'column'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'table' => 'Table',
            'column' => 'Column',
            'state' => 'State',
        ];
    }
}
