<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no minimum-scale=1 maximum-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-custom navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Главная', 'url' => ['/site/index']],
        ['label' => 'Web услуги', 'url' => ['site/internet']],
        ['label' => 'Наружная рекламма', 'url' => ['site/outdoor']],
        ['label' => 'Контакты', 'url' => ['/site/contact']],
        ['label' => 'Полиграфия', 'url' => ['/site/polygraphy']],
        ['label' => 'Отзывы', 'url' => ['/site/reviews']],
        ['label' => 'О нас', 'url' => ['/site/about']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>
<div class="button-up">
<!--    <span class="glyphicon glyphicon-arrow-up chevron_display_up"></span>-->
<!--    <span class="glyphicon glyphicon-arrow-down chevron_display_down"></span>-->
    <span class="glyphicon glyphicon-chevron-up chevron_display_up"></span>
    <span class="glyphicon glyphicon-chevron-down chevron_display_down"></span>
</div>
<!--data-toggle="modal" data-target="#feedback-modal"-->
<div class="button-feedback drag">
    <i class="glyphicon glyphicon-earphone"></i>
</div>
<!--<a href="#" class="button-feedback drag" data-toggle="modal" data-target="#feedback-modal">-->
<!--    <i class="glyphicon glyphicon-earphone"></i>-->
<!--</a>-->
<?= Modal::widget([
    'class' => 'modal',
    'id' => 'feedback-modal',
    'size' => Modal::SIZE_SMALL,
    'header' => 'Обратная связь',
    'footer' => Html::button('Назад', [
            'class' => 'btn btn-danger pull-right',
            'data-dismiss' => 'modal'
    ]),
    'closeButton' => ['tag' => 'button', 'label' => '&times;'],
]);?>
<footer class="footer">
    <div class="container">
        <div class="row footer-content">
            <div class="col-xs-12 col-md-5 col-lg-5 center-div">
                <div class="title">Связь с нами</div>
                <div class="icons">
                    <a href="#"><img class="img-responsive" src="/img/footer/vk.png" alt="VK"></a>
                    <a href="#"><img class="img-responsive" src="/img/footer/facebook.png" alt="Facebook"></a>
                    <a href="#"><img class="img-responsive" src="/img/footer/gmail.png" alt="Gmail"></a>
                    <a href="#"><img class="img-responsive" src="/img/footer/mail.png" alt="Mail"></a>
                    <a href="#"><img class="img-responsive" src="/img/footer/rss.png" alt="RSS"></a>
                </div>
            </div>
            <div class="col-xs-12 col-md-7 col-lg-7 center-div">
                <div class="title">Контакты</div>
                <div class="info">
                    <ul>
                        <li><span class="glyphicon glyphicon-earphone"></span> +38(800)5553535</li>
                        <li><span class="glyphicon glyphicon-envelope"></span> test@test.com</li>
                        <li><span class="glyphicon glyphicon-credit-card">&#x20b4;</span> 8464 6545 4645 1301 1348</li>
                        <li></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="fixed-bottom footer-down col-lg-12 col-xs-12 col-md-12 col-sm-12">
        Создано с любовью для ваc от самых кул разрабов мира - С. Скромность ;)
    </div>
</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
