<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `customer`.
 */
class m180613_185044_create_customer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('customer', [
            'id' => Schema::TYPE_PK,
            'first_name' => Schema::TYPE_STRING.' NOT NULL',
            'second_name' => Schema::TYPE_STRING.' NOT NULL',
            'patronymic' => Schema::TYPE_STRING.' NOT NULL',
            'email' => Schema::TYPE_STRING.' NOT NULL',
            'phone' => Schema::TYPE_STRING.' NOT NULL',
            'second_phone' => Schema::TYPE_STRING,
            'address' => Schema::TYPE_STRING.'(512)',
            'count_order' => Schema::TYPE_INTEGER.' DEFAULT 0',
            'created_at' => Schema::TYPE_STRING.' NOT NULL'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('customer');
    }
}
