<?php

namespace backend\models;

use Yii;
use common\models\Profile;

/**
 * This is the model class for table "customer".
 *
 * @property int $id
 * @property string $full_name
 * @property string $email
 * @property string $phone
 * @property string $second_phone
 * @property int $count_order
 * @property string $created_at
 *
 * @property Order[] $orders
 * @property Profile $profile
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['full_name', 'email', 'phone', 'created_at'], 'required'],
            [['count_order'], 'integer'],
            [['full_name', 'email', 'phone', 'second_phone', 'created_at'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'Full Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'second_phone' => 'Second Phone',
            'count_order' => 'Count Order',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['customer_id' => 'id']);
    }
}
