<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 27.06.2018
 * Time: 15:56
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use backend\models\Paper;
use backend\models\TypePrint;
use backend\models\TypeOrder;
use kartik\datetime\DateTimePicker;

/* @var $this \yii\web\view */
$this->registerJsFile(Yii::getAlias('@web') . '/js/get-price.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>

<?= Html::dropDownList('','', [
    '0' => 'Простой заказ',
    '1' => 'Важный заказ',
    '2' => 'Срочный заказ',
],['class' => 'form-control order-status']) ?>

<?= Html::dropDownList('','', ArrayHelper::map(TypeOrder::find()->all(), 'id', 'type'),[
    'prompt' => 'Тип заказа',
    'class' => 'form-control order-type'
]) ?>

<?= Html::radioList('', '', [
    '0' => 'без дизайна',
    '1' => 'с дизайном',
], ['class' => 'order-service'])?>

<?= Html::dropDownList('','', ArrayHelper::map(Paper::find()->all(), 'id', 'type'),[
    'prompt' => 'Тип бумаги',
    'class' => 'form-control order-paper'
])?>

<?= Html::dropDownList('','', ArrayHelper::map(TypePrint::find()->all(), 'id', 'type'),[
    'prompt' => 'Тип печати',
    'class' => 'form-control order-type-print'
])?>

<?= Html::dropDownList('','', [
    '1' => 'односторонняя печать',
    '2' => 'двухсторонняя печать',
],['class' => 'form-control order-sides']) ?>

    <?= Html::label('Тираж'); ?>
    <?= Html::textInput('','',['class' => 'form-control order-edition']) ?>
    <?= Html::label('Цена'); ?>
    <div class="price-calculator-block">
        <div class="price-calculator-input">
            <?= Html::textInput('', '',['maxlength' => true, 'class' => 'form-control order-price']) ?>
        </div>
        <div class="price-calculator-input-btn">
            <button type="button" class="price-order-btn btn btn-success" data-id="<?= $model->id ?>"><span class="glyphicon glyphicon-retweet"></span></button>
        </div>
    </div>

