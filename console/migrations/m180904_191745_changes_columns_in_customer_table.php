<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180904_191745_changes_columns_in_customer_table
 */
class m180904_191745_changes_columns_in_customer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('customer', 'first_name');
        $this->dropColumn('customer', 'second_name');
        $this->dropColumn('customer','patronymic');
        $this->addColumn('customer', 'full_name', Schema::TYPE_STRING.' NOT NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('customer','full_name');
        $this->addColumn('customer', 'first_name', Schema::TYPE_STRING.' NOT NULL');
        $this->addColumn('customer', 'second_name', Schema::TYPE_STRING.' NOT NULL');
        $this->addColumn('customer', 'patronymic', Schema::TYPE_STRING);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180904_191745_changes_columns_in_customer_table cannot be reverted.\n";

        return false;
    }
    */
}
