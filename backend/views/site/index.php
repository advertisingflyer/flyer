<?php

use yii\widgets\ListView;
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';

?>
<div class="site-index">
<div class="news-block">
    <div class="news-control">
        <?= Html::a('Добавить новость', ['/admin-news/create'], ['class' => 'btn btn-default']) ?>
    </div>
    <div class="news-data">
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'summary' => '',
            'options' => ['data-pjax'=> true],
            'itemOptions' => ['class' => 'item'],
            'itemView' => function ($model) {
                return $this->renderAjax('_data_news',[
                    'model' => $model
                ]);
            },
        ]) ?>
    </div>
</div>
<div class="calculator-block">
    <div class="news-control">
        <h4>Калькулятор заказа</h4>
    </div>
    <div class="calculator-data">
        <?= $this->render('_data_calculator'); ?>
    </div>
</div>

</div>
