$('.price-order-btn').on('click', function () {
    let type = $('.order-type').val();
    let paper = $('.order-paper').val();
    let typePrint = $('.order-type-print').val();
    let service = $('.order-service').val();
    let edition = $('.order-edition').val();
    let sides = $('.order-sides').val();
    $.ajax(
        {
            url : '/create-order/get-price',
            type: "POST",
            data : {type: type, paper: paper, typePrint: typePrint, edition: edition, sides: sides, service: service},
            success: function(res) {
                $('.order-price').val(res);
            },
            error: function(res) {
                console.log(res);
            }
        }
    );
});