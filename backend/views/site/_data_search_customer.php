<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 05.09.2018
 * Time: 23:10
 */

use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $model \backend\models\Customer */

?>

<div class="customer-search-block">
    <div class="customer-search-info">
        <p><?= $model->full_name ?></p>
        <p>Телефон: <?= $model->phone ?></p>
        <p>Количество заказов: <?= count($model->orders); ?></p>
    </div>
    <a href="<?= Url::to(['/customer/view', 'id' => $model->id]) ?>">
        <div class="customer-search-btn link-detected">
            Перейти <span class="glyphicon glyphicon-chevron-right"></span>
        </div>
    </a>
</div>

