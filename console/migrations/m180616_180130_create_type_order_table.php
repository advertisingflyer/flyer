<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `type_order`.
 */
class m180616_180130_create_type_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('type_order', [
            'id' => Schema::TYPE_PK,
            'type' => Schema::TYPE_STRING.' NOT NULL',
            'price' => Schema::TYPE_MONEY
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('type_order');
    }
}
