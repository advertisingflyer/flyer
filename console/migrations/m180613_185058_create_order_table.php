<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 */
class m180613_185058_create_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order', [
            'id' => Schema::TYPE_PK,
            'customer_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'type' => Schema::TYPE_STRING.' NOT NULL',
            'paper' => Schema::TYPE_INTEGER.' NOT NULL',
            'type_print' => Schema::TYPE_INTEGER.' NOT NULL',
            'about' => Schema::TYPE_STRING.'(8196)',
            'price' => Schema::TYPE_MONEY,
            'status' => Schema::TYPE_SMALLINT,
            'image' => Schema::TYPE_STRING,
            'created_at' => Schema::TYPE_STRING.' NOT NULL',
            'deadline' => Schema::TYPE_STRING,
            'completed' => Schema::TYPE_STRING,
        ]);

        $this->addForeignKey(
            'key_order_customer',
            'order',
            'customer_id',
            'customer',
            'id'
        );

        $this->addForeignKey(
            'key_order_paper',
            'order',
            'paper',
            'paper',
            'id',
            'no action',
            'cascade'
        );

        $this->addForeignKey(
            'key_order_type_print',
            'order',
            'type_print',
            'type_print',
            'id',
            'no action',
            'cascade'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('key_order_type_print','order');
        $this->dropForeignKey('key_order_paper','order');
        $this->dropForeignKey('key_order_customer','order');
        $this->dropTable('order');
    }
}
