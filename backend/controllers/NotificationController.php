<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 29.06.2018
 * Time: 16:16
 */

namespace backend\controllers;


use yii\web\Controller;
use common\models\Feedback;
use backend\models\Order;
use yii\data\ActiveDataProvider;

class NotificationController extends Controller
{
    public function actionGetNotification()
    {
        $feedback = Feedback::find();
        $order = Order::find();

        $dataProviderFeedback = new ActiveDataProvider([
            'query' => $feedback->where(['answer' != null])
        ]);

        $dataProviderOrder = new ActiveDataProvider([
            'query' => $order->where(['state' => 0])
        ]);

        return $this->renderAjax('notification',[
            'dataProviderFeedback' => $dataProviderFeedback,
            'dataProviderOrder' => $dataProviderOrder
        ]);
    }
}