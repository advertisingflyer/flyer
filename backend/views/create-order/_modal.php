<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.2018
 * Time: 13:38
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use backend\models\Paper;
use backend\models\TypePrint;
use backend\models\TypeOrder;
use kartik\datetime\DateTimePicker;

/* @var $this \yii\web\view */
/* @var $model \backend\models\Order */
/* @var $form \yii\widgets\ActiveForm */
/* @var $modal string */

?>
<div class="display-modal" id="display-modal">
    <?php $form = ActiveForm::begin([
        'action' => ['/create-order/'.$modal.'-order']
    ]); ?>

    <?= Html::hiddenInput('id', $model->id) ?>
    <?= Html::hiddenInput('customer_id', $model->customer_id) ?>

    <?= $form->field($model, 'type')->dropDownList(ArrayHelper::map(TypeOrder::find()->all(), 'id', 'type'),[
        'prompt' => 'Тип заказа',
        'class' => 'form-control order-type'
    ])->label(false) ?>

    <?= $form->field($model, 'service')->radioList([
        '0' => 'без дизайна',
        '1' => 'с дизайном',
    ], ['class' => 'order-service'])->label(false) ?>

    <?= $form->field($model, 'paper')->dropDownList(ArrayHelper::map(Paper::find()->all(), 'id', 'type'),[
        'prompt' => 'Бумага',
        'class' => 'form-control order-paper'
    ])->label(false) ?>

    <?= $form->field($model, 'type_print')->dropDownList(ArrayHelper::map(TypePrint::find()->all(), 'id', 'type'),[
        'prompt' => 'Тип печати',
        'class' => 'form-control order-type-print'
    ])->label(false) ?>

    <?= $form->field($model, 'sides')->dropDownList([
        '1' => 'односторонняя печать',
        '2' => 'двухсторонняя печать',
    ],['class' => 'form-control order-sides'])->label(false) ?>

    <?= $form->field($model, 'edition')->textInput(['class' => 'form-control order-edition']) ?>

    <div class="price-order-block">
        <div class="price-order-input">
            <?= $form->field($model, 'price')->textInput(['maxlength' => true, 'class' => 'form-control order-price']) ?>
        </div>
        <div class="price-order-input-btn">
            <button type="button" class="price-order-btn btn btn-success" data-id="<?= $model->id ?>"><span class="glyphicon glyphicon-retweet"></span></button>
        </div>
    </div>

    <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([
        '0' => 'Простой заказ',
        '1' => 'Важный заказ',
        '2' => 'Срочный заказ',
    ]) ?>

    <?= $form->field($model, 'deadline')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Enter event time ...','class' => 'form-control modal-sm',],
        'name' => 'dp_4'.$model->id,
        'id' => $model->id,
        'language' => 'ru-RU',
        'type' => DateTimePicker::TYPE_INLINE,
        'pluginOptions' => [
            'format' => 'D, dd-M-yyyy, hh:ii',
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary clone-btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
