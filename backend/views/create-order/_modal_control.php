<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 07.07.2018
 * Time: 17:06
 */

use yii\helpers\Html;

/* @var $this \yii\web\view */
/* @var $model \backend\models\Order */

?>
<?php if ($model->state == 2): ?>
    <div class="form-group">
        <?= Html::button('<span class="glyphicon glyphicon-play"></span> Восстановить заказ', ['class' => 'btn btn-save pause-order-btn form-control', 'data-id' => $model->id]); ?>
    </div>
<?php elseif ($model->state != 3): ?>
    <div class="form-group">
        <?= Html::button('<span class="glyphicon glyphicon-pause"></span> Приостановить заказ', ['class' => 'btn btn-pause pause-order-btn form-control', 'data-id' => $model->id]); ?>
    </div>
<?php endif; ?>
<div class="form-group">
    <?= Html::button('<span class="glyphicon glyphicon-remove"></span> Отменить заказ', ['class' => 'btn btn-canceled canceled-order-btn form-control', 'data-id' => $model->id]); ?>
</div>
