<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180616_180503_add_column_edition_in_order_table
 */
class m180616_180503_add_column_edition_in_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order','edition', Schema::TYPE_INTEGER.' NOT NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order','edition');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180616_180503_add_column_edition_in_order_table cannot be reverted.\n";

        return false;
    }
    */
}
