<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 13.06.2018
 * Time: 23:46
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\AutoComplete;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Customer */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Create customer';
$this->registerJsFile(Yii::getAlias('@web') . '/js/customer.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>

<div class="customer-form">
    <h2 class="title-customer">Оформление клиента</h2>
    <div class="customer-block">
        <?php $form = ActiveForm::begin(); ?>

        <div class="full-name-block">
            <div class="customer-elem">
                <span class="customer-label">Фамилия</span>
                <?= AutoComplete::widget([
                    'name' => 'second_name',
                    'options' => ['class' => 'form-control'],
                    'clientOptions' => [
                        'source' => Url::to(['create-order/second-name']),
                        'minLength'=>'1',
                        'limit' => '1'
                    ],
                ]); ?>
            </div>
            <div class="customer-elem">
                <span class="customer-label">Имя</span>
                <?= AutoComplete::widget([
                    'name' => 'first_name',
                    'options' => ['class' => 'form-control'],
                    'clientOptions' => [
                        'source' => Url::to(['create-order/first-name']),
                        'minLength'=>'1',
                        'limit' => '1'
                    ],
                ]); ?>
            </div>
            <div class="customer-elem">
                <span class="customer-label">Отчество</span>
                <?= AutoComplete::widget([
                    'name' => 'patronymic',
                    'options' => ['class' => 'form-control'],
                    'clientOptions' => [
                        'source' => Url::to(['create-order/patronymic']),
                        'minLength'=>'1',
                        'limit' => '1'
                    ],
                ]); ?>
            </div>
        </div>

        <div class="customer-info">
            <span class="customer-label">Email</span>
            <?= Html::textInput('email', '',['class' => 'form-control']) ?>
            <span class="customer-label">Телефон</span>
            <?= AutoComplete::widget([
                'name' => 'phone',
                'options' => ['class' => 'form-control customer-phone'],
                'clientOptions' => [
                    'source' => Url::to(['create-order/phone']),
                    'minLength'=>'1',
                    'limit' => '1'
                ],
            ]); ?>
            <span class="customer-label">Доп. телефон</span>
            <?= Html::textInput('second_phone', '',['class' => 'form-control']) ?>
        </div>

        <div class="form-group customer-form-btn">
            <?= Html::submitButton('Добавить клиента', ['class' => 'btn btn-save']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <div class="customer-search">
        <button class="btn btn-save customer-btn-search" ><span class="glyphicon glyphicon-search"></span></button>
    </div>
   <div class="customer-block">
       <div class="customer-order-title">История клиента</div>
       <div class="customer-order">
           <div class="wrapper" id="#loading">
               <div class="inner">
                   <span>З</span>
                   <span>а</span>
                   <span>г</span>
                   <span>р</span>
                   <span>у</span>
                   <span>з</span>
                   <span>к</span>
                   <span>а</span>
               </div>
           </div>
       </div>
   </div>

</div>

