$('.button-feedback').on('click', function () {
    $('#feedback-modal').modal('show');
    $.ajax(
        {
            url: '/feedback/show',
            type: 'POST',
            success: function (response) {
                $('.modal-body').html(response);
            },
            error: function (response) {
                $('.modal-body').html('Error');
            }
    });
});

$(document).on('beforeSubmit', '#phone-form', function (e) {
    $.ajax({
        url: '/feedback/phone',
        type: "POST",
        data: $('#phone-form').serialize(),
        success: function(res){
            $('#feedback-modal').modal('show');
            $('.modal-body').html(res);
        },
        error: function (res) {
            $('#feedback-modal').modal('show');
            $('.modal-body').html(res);
        }
    });
    return false;
});

$(document).on('beforeSubmit', '#question-form', function (e) {
    $.ajax({
            url: '/feedback/question',
            type: "POST",
            data: $('#question-form').serialize(),
            success: function(res){
                $('#feedback-modal').modal('show');
                $('.modal-body').html(res);
            },
            error: function (res) {
                $('#feedback-modal').modal('show');
                $('.modal-body').html(res);
            }
        });
    return false;
});