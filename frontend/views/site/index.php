<?php

use yii\helpers\Html;
use yii\bootstrap\Carousel;

/* @var $this \yii\web\view */
/* @var $model \backend\models\Carousel */
/* @var $count integer */

$this->title = 'Главная';
?>
<div class="site-index">

    <?= Carousel::widget([
        'items' => $items,
        'options' => ['class' => 'carousel slide', 'data-interval' => '5000'],
        'controls' => [
            '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>',
            '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>'
        ]
    ]); ?>
</div>