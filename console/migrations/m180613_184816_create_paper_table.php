<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `paper`.
 */
class m180613_184816_create_paper_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('paper', [
            'id' => Schema::TYPE_PK,
            'type' => Schema::TYPE_STRING.' NOT NULL',
            'price' => Schema::TYPE_MONEY
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('paper');
    }
}
