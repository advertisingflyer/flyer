<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 04.09.2018
 * Time: 21:36
 */

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $model \backend\models\Order */

?>
<div class="customer-search-block">
    <div class="customer-search-info">
        Свойства заказа:
            <p><span class="badge"><?= $model->typeOrder->type ?></span>
            <span class="badge"><?= $model->typePaper->type ?></span>
            <span class="badge"><?= $model->typePrint->type ?></span></p>
            <?php if ($model->sides == 1): ?>
                <span class="badge">односторонняя печать</span>
            <?php else: ?>
                <span class="badge">двухсторонняя печать</span>
            <?php endif; ?>
        <p>Заказчик: <?= Html::a($model->customer->full_name, ['/customer/view', 'id' => $model->customer_id],
                ['class' => 'link-detected badge']) ?></p>
        <?php if ($model->state == 2): ?>
            <p><span class="badge badge-order-warning">Приостановлен: <b>Deadline:</b> <?= $model->deadline ?></span></p>
        <?php else: ?>
                <td><b>Deadline:</b>
                <?= $model->deadline ?>
            <?php if ($model->state == 3): ?>
                <p><span class="badge badge-order-canceled"><b>Отменен:</b> <?= $model->completed ?></span></p>
            <?php else: ?>
                <?php if ($model->completed != null): ?>
                    <p><span class="badge badge-order-success"><b>Выполнен:</b> <?= $model->completed ?></span></p>
                <?php else: ?>
                    <p><span class="badge badge-order-wait"><b>Выполняется</b></span></p>
                <?php endif; ?>
            <?php endif; ?>
        <?php endif; ?>
    </div>
    <a href="<?= Url::to(['/create-order/order-success', 'id' => $model->id]) ?>">
        <div class="customer-search-btn link-detected">
            Перейти <span class="glyphicon glyphicon-chevron-right"></span>
        </div>
    </a>
</div>