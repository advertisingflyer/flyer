<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\bootstrap\ActiveForm;
use yii\jui\AutoComplete;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="header">
        <?php if (!Yii::$app->user->isGuest): ?>
            <div id="menu-action">
                <i class="glyphicon glyphicon-align-justify"></i>
            </div>
        <?php endif; ?>
        <div class="menu-name">
            <?= Html::a('Admin panel', ['/site/index'], ['class' => 'menu-name']); ?>
        </div>
        <?php if (!Yii::$app->user->isGuest): ?>
        <?php ActiveForm::begin(
                [
                    'action' => ['/site/search'],
                    'method' => 'get',
                    'options' => [
                        'class' => 'nav-search'
                    ]
                ]
            );
            echo '<div class="input-group input-group-sm">';
//            echo Html::input(
//                'type: text',
//                'search',
//                '',
//                [
//                    'placeholder' => 'Поиск',
//                    'class' => 'form-control nav-search-input'
//                ]
//            );
            echo AutoComplete::widget([
                    'name' => 'search',
                    'options' => ['class' => 'form-control nav-search-input'],
                    'clientOptions' => [
                        'source' => Url::to(['/site/search-auto']),
                        'minLength'=>'1',
                        'limit' => '1'
                    ],
                ]);
            echo '<span class="input-group-btn">';
            echo Html::submitButton(
                '<span class="glyphicon glyphicon-search"></span>',
                [
                    'class' => 'btn btn-nav-search',
                    'onClick' => 'window.location.href = this.form.action + "?search=" + this.form.search.value.replace(/[^\w\а-яё\А-ЯЁ]+/g, " ");'
                ]
            );
            echo '</span></div>';
            ActiveForm::end(); ?>
        <div class="bell">
            <span class="glyphicon glyphicon-bell"></span>
        </div>
        <?php endif; ?>
        <?php if (!Yii::$app->user->isGuest): ?>
        <div class="menu-user">
            <div class="dropdown">
                <span class="glyphicon glyphicon-user"></span>
                <div class="dropdown-content">
                    <ul>
                        <li><a href="<?= Url::to('@goApp') ?>"><span class="glyphicon glyphicon-home"></span> <span>На главную страницу</span></a></li>
                        <li><a href="<?= Url::to(['/site/logout']) ?>" data-method="post"><span class="glyphicon glyphicon-log-out"></span> <span>Выход</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
    <?php if (!Yii::$app->user->isGuest): ?>
        <div class="sidebar">
            <ul>
                <li><a href="<?= Url::to(['/create-order/customer']) ?>" class="menu-a"><i class="glyphicon glyphicon-plus"></i><span class="menu-name-a">Create order</span></a></li>
                <li><a href="#" class="menu-a" data-id="side-block-pages"><i class="glyphicon glyphicon-folder-open"></i><span class="menu-name-a">Pages</span></a></li>
                <li><a href="#" class="menu-a" data-id="side-block-tables"><i class="glyphicon glyphicon-th-list"></i><span class="menu-name-a">Server</span></a></li>
                <li><a href="#" class="menu-a"><i class="fa fa-envelope-o"></i><span class="menu-name-a">Messages</span></a></li>
                <li><a href="#" class="menu-a"><i class="fa fa-table"></i><span class="menu-name-a">Data Table</span></a></li>
            </ul>
        </div>
        <div class="side-block-pages">
            <div class="side-bl-tx">Pages</div>
            <a href="<?= Url::to(['/carousel/']) ?>" class="side-item-a">
                <div class="side-item">
                    Слайдер
                </div>
            </a>
            <a href="<?= Url::to(['/web-page/']) ?>" class="side-item-a">
                <div class="side-item">
                    Web услуги
                </div>
            </a>
            <a href="<?= Url::to(['/outdoor-advertising-page/']) ?>" class="side-item-a">
                <div class="side-item">
                    Наружная реклама
                </div>
            </a>
        </div>
        <div class="side-block-tables">
            <div class="side-bl-tx">Tables</div>
            <a href="<?= Url::to(['/reviews/']) ?>" class="side-item-a">
                <div class="side-item">
                    Reviews
                </div>
            </a>
            <a href="<?= Url::to(['/customer/']) ?>" class="side-item-a">
                <div class="side-item">
                    Customers
                </div>
            </a>
            <a href="<?= Url::to(['/order/']) ?>" class="side-item-a">
                <div class="side-item">
                    Orders
                </div>
            </a>
            <a href="<?= Url::to(['/type-order/']) ?>" class="side-item-a">
                <div class="side-item">
                    Type order
                </div>
            </a>
            <a href="<?= Url::to(['/paper/']) ?>" class="side-item-a">
                <div class="side-item">
                    Paper
                </div>
            </a>
            <a href="<?= Url::to(['/type-print/']) ?>" class="side-item-a">
                <div class="side-item">
                    Type print
                </div>
            </a>
            <a href="<?= Url::to(['/keyword/']) ?>" class="side-item-a">
                <div class="side-item">
                    Keyword
                </div>
            </a>
        </div>
    <?php endif; ?>
    <div class="content-view">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
