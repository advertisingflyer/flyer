<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\AdminNews */

$this->title = 'Create Admin News';
$this->params['breadcrumbs'][] = ['label' => 'Admin News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-news-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
