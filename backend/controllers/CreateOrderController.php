<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 13.06.2018
 * Time: 23:42
 */

namespace backend\controllers;


use backend\models\Paper;
use backend\models\TypeOrder;
use backend\models\TypePrint;
use Yii;
use yii\web\Controller;
use backend\models\Customer;
use backend\models\Order;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;

class CreateOrderController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['customer', 'order', 'order-success', 'canceled-order', 'suspend-order'],
                'rules' => [
                    [
                        'actions' => ['customer','order','order-success', 'canceled-order', 'suspend-order'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionCustomer()
    {
        $model = new Customer();

        $secondName = Yii::$app->request->post('second_name');
        $firstName = Yii::$app->request->post('first_name');
        $patronymic = Yii::$app->request->post('patronymic');
        $email = Yii::$app->request->post('email');
        $phone = Yii::$app->request->post('phone');
        $secondPhone = Yii::$app->request->post('second_phone');

        if ($firstName != null && $secondName != null && $patronymic != null
        && $email != null && $phone != null)
        {
            $model->full_name = $secondName.' '.$firstName.' '.$patronymic;
            $model->email = $email;
            $model->phone = $phone;
            $model->second_phone = $secondPhone;
            $model->created_at = date(' d/m/Y H:i:s');
            if ($model->save())
            {
                return $this->redirect(['order', 'id' => $model->id]);
            }
        }

        return $this->render('customer',[
            'model' => $model
        ]);
    }

    public function actionOrder($id)
    {
        $model = new Order();

        $model->customer_id = Yii::$app->request->post('customer_id');
        $model->created_at = date('d/m/Y H:i:s');
        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            return $this->redirect(['order-success', 'id' => $model->id]);
        }

        return $this->render('order',[
            'model' => $model,
            'id' => $id
        ]);
    }

    public function actionCloneOrder()
    {
        $model = new Order();

        $model->customer_id = Yii::$app->request->post('customer_id');
        $model->created_at = date('d/m/Y H:i:s');

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            return $this->redirect(['order-success', 'id' => $model->id]);
        }

        $errorMsg = 'Клонирование не возможно';
        return $errorMsg;
    }

    public function actionEditOrder()
    {
        $id = Yii::$app->request->post('id');

        $model = Order::findOne(['id' => $id]);

        $model->customer_id = Yii::$app->request->post('customer_id');
        $model->updated_at = date('d/m/Y H:i:s');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['order-success', 'id' => $model->id]);
        }

        $errorMsg = 'Редактирование не возможно';
        return $errorMsg;
    }

    public function actionOrderSuccess($id)
    {
        $model = Order::findOne(['id' => $id]);

        return $this->render('order-success',[
            'model' => $model
        ]);
    }

    public function actionGetPrice()
    {
        $type = Yii::$app->request->post('type');
        $paper = Yii::$app->request->post('paper');
        $typePrint = Yii::$app->request->post('typePrint');
        $edition = Yii::$app->request->post('edition');
        $sides = Yii::$app->request->post('sides');
        $service = Yii::$app->request->post('service');

        $type = TypeOrder::findOne(['id' => $type])->price;
        $paper = Paper::findOne(['id' => $paper])->price;
        $typePrint = TypePrint::findOne(['id' => $typePrint])->price;

        $price = (($paper + $typePrint) * ($edition * $sides)) + $type;

        return json_encode($price);
    }

    public function actionGetModal()
    {
        $orderID = Yii::$app->request->post('order');
        $modalType = Yii::$app->request->post('type');
        $model = Order::findOne(['id' => $orderID]);

        return $this->renderAjax('_modal', [
            'model' => $model,
            'modal' => $modalType
        ]);
    }

    public function actionCustomerHistory()
    {
        $customer = Yii::$app->request->post('customer');

        $customer = Customer::findOne(['phone' => $customer]);
        if ($customer != null)
        {
            $query = Order::find();

            $dataProvider = new ActiveDataProvider([
                'query' => $query->where(['customer_id' => $customer->id]),
                'sort' => ['defaultOrder'=>['id' => SORT_DESC]],
                'pagination' => false
            ]);

            return $this->renderAjax('customer_history',[
                'dataProvider' => $dataProvider,
                'customer' => $customer
            ]);
        }
        $errorMsg = 'Такого клиента не существует';
        return $errorMsg;
    }

    public function actionPayModal()
    {
        $orderID = Yii::$app->request->post('order');

        $model = Order::findOne(['id' => $orderID]);

        return $this->renderAjax('_modal_pay', [
            'model' => $model
        ]);
    }

    public function actionSetPaid()
    {
        $amount = Yii::$app->request->post('amount');
        $orderID = Yii::$app->request->post('order');

        $model = Order::findOne(['id' => $orderID]);

        $model->paid_out += $amount;

        $model->save();

        return $this->renderAjax('_modal_pay', [
            'model' => $model
        ]);
    }

    public function actionCloseOrder()
    {
        $orderID = Yii::$app->request->post('order');

        $model = Order::findOne(['id' => $orderID]);

        $model->state = 1;
        $model->completed = date('d/m/Y H:i:s');
        $model->save();

        return $this->redirect(['order-success', 'id' => $model->id]);
    }

    public function actionControlModal()
    {
        $orderID = Yii::$app->request->post('order');

        $model = Order::findOne(['id' => $orderID]);

        return $this->renderAjax('_modal_control', [
            'model' => $model
        ]);
    }

    public function actionSuspendOrder()
    {
        $orderID = Yii::$app->request->post('order');

        $model = Order::findOne(['id' => $orderID]);

        if ($model->state == 2)
        {
            $model->state = 0;
        } else {
            $model->state = 2;
        }

        $model->save();

        return $this->redirect(['order-success', 'id' => $model->id]);
    }

    public function actionPrintOrder()
    {
        $id = Yii::$app->request->get('id');

        $model = Order::findOne(['id'=>$id]);

        $content = $this->renderPartial('_data_print', [
            'model' => $model,
        ]);
        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_UTF8,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:18px}',
            'options' => [
                'title' => 'Заказ на имя '.$model->customer->full_name,
                'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
            ],
            'methods' => [
                'SetHeader' => ['flyer.loc||Generated On: ' . date("r")],
//                'SetFooter' => ['|Page {PAGENO}|'],
            ]
        ]);
        return $pdf->render();
    }

    public function actionCanceledOrder()
    {
        $orderID = Yii::$app->request->post('order');

        $model = Order::findOne(['id' => $orderID]);

        $model->state = 3;
        $model->completed = date('d/m/Y H:i:s');
        $model->save();

        return $this->redirect(['order-success', 'id' => $model->id]);
    }

    public function actionFirstName()
    {
        $qq = Yii::$app->request->get('term');
        $query = Customer::find()->select(['full_name as value'])->where(['like','full_name', $qq])
            ->asArray('title')
            ->all();
        $result = array();
        foreach($query as $q){
            if (strstr($q['value'], $qq)) {
                $result[] = array('value' => explode(' ', $q['value'])[1]);
            }
        }
        return json_encode($result);
    }

    public function actionSecondName()
    {
        $qq = Yii::$app->request->get('term');
        $query = Customer::find()->select(['full_name as value'])->where(['like','full_name', $qq])
            ->asArray('title')
            ->all();
        $result = array();
        foreach($query as $q){
            if (strstr($q['value'], $qq)) {
                $result[] = array('value' => explode(' ', $q['value'])[0]);
            }
        }
        return json_encode($result);
    }

    public function actionPatronymic()
    {
        $qq = Yii::$app->request->get('term');
        $query = Customer::find()->select(['full_name as value'])->where(['like','full_name', $qq])
            ->asArray('title')
            ->all();
        $result = array();
        foreach($query as $q){
            if (strstr($q['value'], $qq)) {
                $result[] = array('value' => explode(' ', $q['value'])[2]);
            }
        }
        return json_encode($result);
    }

    public function actionPhone()
    {
        $qq = Yii::$app->request->get('term');
        $query = Customer::find()->select(['phone as value'])->where(['like','phone', $qq])
            ->asArray('title')
            ->all();
        $result = array();
        foreach($query as $q){
            if (strstr($q['value'], $qq)) {
                $result[] = array('value' => $q['value']);
            }
        }
        return json_encode($result);
    }
}