<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15.06.2018
 * Time: 15:55
 */

use yii\widgets\ListView;
use yii\helpers\Html;

/* @var $this \yii\web\view */
/* @var $customer \backend\models\Customer */
$this->registerJsFile(Yii::getAlias('@web') . '/js/get-modal.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>
<div class="customer-data-order">
    <div class="customer-history-control">
        <p><b>Ф.И.О.:  <?= $customer->full_name ?></b></p>
        <?= Html::a('Добавить заказ', ['/create-order/order', 'id' => $customer->id], ['class' => 'btn btn-add-order']); ?>
        <?= Html::a('<span class="glyphicon glyphicon-info-sign"></span> Перейти к клиенту', ['/customer/view', 'id' => $customer->id], ['class' => 'btn btn-add-order']) ?>
    </div>
    <div class="order-history">
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'summary' => '',
            'options' => ['data-pjax'=> true],
            'itemOptions' => ['class' => 'item'],
            'itemView' => function ($model) {
                return $this->renderAjax('_data_history',[
                    'model' => $model
                ]);
            },
        ]) ?>
    </div>
</div>

