<?php

namespace frontend\assets;

use yii\helpers\Html;
use yii\web\AssetBundle;
use yii\web\View;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/footer.css',
        'css/navbar.css',
        'css/upButton.css',
        'css/feedback.css',
        'css/carousel.css'
    ];
    public $js = [
        'js/upButton.js',
        'js/feedback.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
