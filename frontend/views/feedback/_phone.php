<?php
/**
 * Created by PhpStorm.
 * User: nikita
 * Date: 15.08.2018
 * Time: 22:06
 */

use yii\bootstrap\Html;

?>
<?php $feedbackForm = \yii\widgets\ActiveForm::begin([
        'class' => 'phone',
        'id' => 'feedback-form-phone',
]);
echo $feedbackForm->field($model, 'name');//->textInput()->label('Ваше имя');
echo $feedbackForm->field($model, 'phone');//->textInput()->label('Телефон'); допилить маску ввода
echo Html::SubmitButton('Заказать звонок', [
    'class' => 'btn btn-success btn-phone',
]);
\yii\widgets\ActiveForm::end(); ?>
