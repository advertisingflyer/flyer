<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 16.06.2018
 * Time: 22:30
 */

/* @var $this \yii\web\view */
/* @var $model \backend\models\Order */

$this->title = 'Order success';

?>

<div class="order-info-block">

    <div class="order-info-header">
        <p>Заказчик: <?= $model->customer->full_name ?></p>
        <p>
            Статус:
            <?php if ($model->status == 0): ?>
                Простой заказ
            <?php elseif ($model->status == 1): ?>
                Важный заказ
            <?php elseif ($model->status == 2): ?>
                Срочный заказ
            <?php endif; ?>
        </p>
    </div>
    <div class="order-info-column">
        <div class="order-info-col-th">
            <table class="order-info-table">
                <tr>
                    <td>Тип заказа:</td>
                    <td><?= $model->typeOrder->type ?></td>
                </tr>
                <tr>
                    <td>Тип бумаги:</td>
                    <td><?= $model->typePaper->type ?></td>
                </tr>
                <tr>
                    <td>Тип печати:</td>
                    <td><?= $model->typePrint->type ?></td>
                </tr>
                <tr>
                    <td>Дополнительные услуги:</td>
                    <td>
                        <?php if ($model->service == 0): ?>
                            без дизайна
                        <?php else: ?>
                            с дизайном
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td>Стороны:</td>
                    <td><?= $model->sides ?></td>
                </tr>
                <tr>
                    <td>Тираж:</td>
                    <td><?= $model->edition ?></td>
                </tr>
                <tr>
                    <td>Цена:</td>
                    <td><?= $model->price ?></td>
                </tr>
            </table>
        </div>
        <div class="order-info-col">
            <table class="order-info-table">
                <tr>
                    <td>Срок:</td>
                    <td><?= $model->deadline ?></td>
                </tr>
                <tr>
                    <td>Создан:</td>
                    <td><?= $model->created_at ?></td>
                </tr>
                <tr>
                    <td>Выплачено:</td>
                    <td><?= $model->paid_out ?></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="order-info-column">
        <div class="order-info-col-th">
            <p>Адрес:</p>
            <?= $model->address ?>
        </div>
        <div class="order-info-col">
            <p>Описание:</p>
            <?= $model->description ?>
        </div>
    </div>
    <div class="order-info-footer">
        <?php if ($model->state == 2): ?>
            <p><span class="badge badge-order-warning">Приостановлен: <b>Deadline:</b> <?= $model->deadline ?></span></p>
        <?php else: ?>
            <?php if ($model->state == 3): ?>
                <p><span class="badge badge-order-canceled"><b>Отменен:</b> <?= $model->completed ?></span></p>
            <?php else: ?>
                <?php if ($model->completed != null): ?>
                    <p><span class="badge badge-order-success"><b>Выполнен:</b> <?= $model->completed ?></span></p>
                <?php else: ?>
                    <p><span class="badge badge-order-wait"><b>Выполняется</b></span></p>
                <?php endif; ?>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>