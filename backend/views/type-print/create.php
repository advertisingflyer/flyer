<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TypePrint */

$this->title = 'Create Type Print';
$this->params['breadcrumbs'][] = ['label' => 'Type Prints', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="type-print-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
