<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\OutdoorAdvertisingPage */

$this->title = 'Create Outdoor Advertising Page';
$this->params['breadcrumbs'][] = ['label' => 'Outdoor Advertising Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="outdoor-advertising-page-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
