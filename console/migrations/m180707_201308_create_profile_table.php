<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `profile`.
 */
class m180707_201308_create_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('profile', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER,
            'customer_id' => Schema::TYPE_INTEGER,
            'created_at' => Schema::TYPE_STRING,
            'updated_at' => Schema::TYPE_STRING,
        ]);

        $this->addForeignKey(
            'key_profile_user',
            'profile',
            'user_id',
            'user',
            'id',
            'cascade',
            'cascade'
        );
        $this->addForeignKey(
            'key_profile_customer',
            'profile',
            'customer_id',
            'customer',
            'id',
            'cascade',
            'cascade'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('key_profile_customer', 'profile');
        $this->dropForeignKey('key_profile_user','profile');
        $this->dropTable('profile');
    }
}
