<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\models\Customer */

$this->title = $model->full_name;

$this->registerJsFile(Yii::getAlias('@web') . '/js/customer/preloader.js', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::getAlias('@web') . '/js/customer/get-modal.js', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::getAlias('@web') . '/js/customer/loader-get-modal.js', ['depends' => [yii\web\JqueryAsset::className()]]);

?>
<div class="customer-view">
    <?php Pjax::begin(); ?>
    <div class="customer-view-block">
        <div class="customer-view-info">
            <table class="customer-view-table">
                <tr><th><span class="badge">Информация о клиенте</span></th></tr>
                <tr>
                    <td><b>Ф.И.О.:</b></td><td><?= $model->full_name ?></td>
                </tr>
                <tr>
                    <td><b>email:</b></td><td><?= $model->email ?></td>
                </tr>
                <tr>
                    <td><b>Телефон:</b></td><td><?= $model->phone ?></td>
                </tr>
                <tr>
                    <td><b>Доп. телефон:</b></td><td><?= $model->second_phone ?></td>
                </tr>
                <tr>
                    <td><b>Профиль:</b></td>
                    <?php if ($model->profile): ?>
                        <td>есть</td>
                    <?php else: ?>
                        <td>нет</td>
                    <?php endif; ?>
                </tr>
                <tr><th><span class="badge">Статистика</span></th></tr>
                <tr>
                    <td><b>Всего заказов:</b></td>
                    <td>
                        <?= $all ?>
                        <?= Html::a(
                            '<span class="glyphicon glyphicon-refresh customer-view-question"></span>',
                            ['/customer/view', 'id' => $model->id, 'state' => ''],
                            ['data-pjax' => true])
                        ?>
                    </td>
                </tr>
                <tr>
                    <td><b>Оплаченные заказы:</b></td>
                    <td>
                        <?= $paidFor ?>
                        <?php if ($paidFor != 0): ?>
                            <?= Html::a(
                                    '<span class="glyphicon glyphicon-question-sign customer-view-question"></span>',
                                    ['/customer/view', 'id' => $model->id, 'state' => 'paidFor'],
                                    ['data-pjax' => true])
                            ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td><b>Приостановленные заказы:</b></td>
                    <td>
                        <?= $suspend ?>
                        <?php if ($suspend != 0): ?>
                            <?= Html::a(
                                '<span class="glyphicon glyphicon-question-sign customer-view-question"></span>',
                                ['/customer/view', 'id' => $model->id, 'state' => 'suspend'],
                                ['data-pjax' => true])
                            ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td><b>Отмененные заказы:</b></td>
                    <td>
                        <?= $canceled ?>
                        <?php if ($canceled != 0): ?>
                            <?= Html::a(
                                '<span class="glyphicon glyphicon-question-sign customer-view-question"></span>',
                                ['/customer/view', 'id' => $model->id, 'state' => 'canceled'],
                                ['data-pjax' => true])
                            ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td><b>Не выполненные:</b></td>
                    <td>
                        <?= $notDone ?>
                        <?php if ($notDone != 0): ?>
                            <?= Html::a(
                                '<span class="glyphicon glyphicon-question-sign customer-view-question"></span>',
                                ['/customer/view', 'id' => $model->id, 'state' => 'notDone'],
                                ['data-pjax' => true])
                            ?>
                        <?php endif; ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="customer-view-control">
            <?= Html::a('Добавить заказ', ['/create-order/order', 'id' => $model->id], ['class' => 'btn btn-add-order', 'data-pjax' => false]); ?>
        </div>
    </div>
    <div class="customer-view-history">
        <div class="wrapper-customer" id="#loading">
            <div class="inner">
                <span>З</span>
                <span>а</span>
                <span>г</span>
                <span>р</span>
                <span>у</span>
                <span>з</span>
                <span>к</span>
                <span>а</span>
            </div>
        </div>
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'summary' => '',
            'options' => ['data-pjax'=> true],
            'itemOptions' => ['class' => 'item'],
            'itemView' => function ($model) {
                return $this->render('_data_history',[
                    'model' => $model
                ]);
            },
        ]) ?>
    </div>
    <?php Pjax::end(); ?>
</div>
