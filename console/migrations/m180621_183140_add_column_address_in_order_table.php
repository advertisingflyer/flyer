<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180621_183140_add_column_address_in_order_table
 */
class m180621_183140_add_column_address_in_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order','address', Schema::TYPE_STRING.'(512)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order','address');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180621_183140_add_column_address_in_order_table cannot be reverted.\n";

        return false;
    }
    */
}
