<?php
/**
 * Created by PhpStorm.
 * User: nikita
 * Date: 15.08.2018
 * Time: 22:15
 */

use yii\helpers\Html;

?>
<?php $feedbackForm = \yii\widgets\ActiveForm::begin([
    'class' => 'question',
    'id' => 'feedback-form'
]);
echo $feedbackForm->field($model, 'name');//->textInput()->label('Ваше имя');
echo $feedbackForm->field($model, 'email');
echo $feedbackForm->field($model, 'question');//->textInput()->label('Уточните ваш вопрос?');
echo Html::SubmitButton('Задать вопрос', [
    'class' => 'btn btn-success btn-question'
]);
\yii\widgets\ActiveForm::end(); ?>
