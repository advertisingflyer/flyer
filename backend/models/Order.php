<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $type
 * @property int $paper
 * @property int $type_print
 * @property string $description
 * @property string $price
 * @property string $paid_out
 * @property int $status
 * @property int $state
 * @property string $image
 * @property string $created_at
 * @property string $updated_at
 * @property string $deadline
 * @property string $completed
 * @property int $edition
 * @property int $sides
 * @property int $service
 * @property string $address
 *
 * @property Customer $customer
 * @property Paper $typePaper
 * @property TypeOrder $typeOrder
 * @property TypePrint $typePrint
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'paper', 'type_print', 'created_at', 'edition'], 'required'],
            [['customer_id', 'type', 'paper', 'type_print', 'status', 'edition', 'sides', 'service', 'state'], 'integer'],
            [['price', 'paid_out'], 'number'],
            [['description'], 'string', 'max' => 8196],
            [['image', 'created_at', 'updated_at', 'deadline', 'completed'], 'string', 'max' => 255],
            [['address'], 'string', 'max' => 512],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['paper'], 'exist', 'skipOnError' => true, 'targetClass' => Paper::className(), 'targetAttribute' => ['paper' => 'id']],
            [['type'], 'exist', 'skipOnError' => true, 'targetClass' => TypeOrder::className(), 'targetAttribute' => ['type' => 'id']],
            [['type_print'], 'exist', 'skipOnError' => true, 'targetClass' => TypePrint::className(), 'targetAttribute' => ['type_print' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'type' => 'Тип заказа',
            'paper' => 'Тип бумаги',
            'type_print' => 'Тип печати',
            'description' => 'Описание заказа',
            'price' => 'Цена',
            'paid_out' => 'Выплачено',
            'status' => 'Статус',
            'state' => 'Состояние',
            'image' => 'Image',
            'created_at' => 'Created At',
            'updated_at' => 'Updated_At',
            'deadline' => 'Deadline',
            'completed' => 'Дата выполнения',
            'edition' => 'Тираж',
            'sides' => 'Количество сторон',
            'service' => 'Сервис',
            'address' => 'Адрес доставки',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypePaper()
    {
        return $this->hasOne(Paper::className(), ['id' => 'paper']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeOrder()
    {
        return $this->hasOne(TypeOrder::className(), ['id' => 'type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypePrint()
    {
        return $this->hasOne(TypePrint::className(), ['id' => 'type_print']);
    }
}
