<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\WebPage */

$this->title = 'Create Web Page';
$this->params['breadcrumbs'][] = ['label' => 'Web Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="web-page-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
