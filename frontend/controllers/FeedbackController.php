<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 29.06.2018
 * Time: 16:10
 */

namespace frontend\controllers;


use common\models\FeedbackPhone;
use common\models\FeedbackQuestion;
use Yii;
use yii\web\Controller;


class FeedbackController extends Controller
{
    public function actionShow()
    {
        $modelPhone = new FeedbackPhone();
        $modelQuestion = new FeedbackQuestion();
        return $this->renderAjax('_modal', [
            'phone' => $modelPhone,
            'question' => $modelQuestion
        ]);
    }

    public function actionPhone()
    {
        $model = new FeedbackPhone();

        if ($model->load(Yii::$app->request->post()))
        {
            $model->created_at = date('h:m:s');
            if($model->save()) {
                return FeedbackPhone::PHONE_SUCCESS;
            }
        }
        else {
            return FeedbackPhone::PHONE_ERROR;
        }

        return $this->renderAjax('phone', [
            'model' => $model
        ]);
    }

    public function actionQuestion()
    {
        $model = new FeedbackQuestion();

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            $model->created_at = date('h:m:s');
            if($model->save())
                return FeedbackQuestion::QUESTION_SUCCESS;
        } else {
            return FeedbackQuestion::QUESTION_ERROR;
        }

        return $this->renderAjax('question', [
            'model' => $model
        ]);
    }
}