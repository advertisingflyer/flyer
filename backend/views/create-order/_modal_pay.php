<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 07.07.2018
 * Time: 15:25
 */

use yii\helpers\Html;

/* @var $this \yii\web\view */
/* @var $model \backend\models\Order */

?>
<div class="display-modal">
    <?php if ($model->price != $model->paid_out): ?>
        <p><b>Необходимая сумма для выполнения:</b> <?= $model->price - $model->paid_out ?></p>
        <p>Введите сумму которую внес клиент:</p>
        <div class="form-group">
            <?= Html::input('text','paid_out'.$model->id,'',['class' => 'form-control order-amount']); ?>
        </div>
        <div class="form-group">
            <?= Html::button('Внести деньги', ['class' => 'btn btn-save form-control btn-set-paid', 'data-id' => $model->id]); ?>
        </div>
    <?php elseif ($model->price == $model->paid_out): ?>
        <?= Html::button('Закрыть заказ',['class' => 'btn btn-save form-control btn-close-order', 'data-id' => $model->id]); ?>
    <?php endif;?>
</div>
