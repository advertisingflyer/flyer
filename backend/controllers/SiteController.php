<?php
namespace backend\controllers;

use backend\models\AdminNews;
use backend\models\Customer;
use backend\models\Keywords;
use backend\models\Order;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'search', 'search-auto'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionSearch()
    {
        $search = Yii::$app->request->get('search');

        $keyword = Keywords::findOne(['key' => explode(' ', $search)]);

        $query = Customer::find();
        $queryOrder = Order::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->orFilterWhere(['like', 'full_name', $search])
            ->orFilterWhere(['like', 'phone', $search])
            ->orFilterWhere(['like', 'email', $search]);

        $dataProviderOrder = new ActiveDataProvider([
            'query' => $queryOrder
        ]);

        $customer = Customer::find()->where(['like', 'full_name', $search])->one();

        $queryOrder->orFilterWhere([$keyword->column => $keyword->state])
            ->orFilterWhere(['customer_id' => $customer['id']]);

        return $this->render('search',
            [
                'query' => $search,
                'resultCustomer' => $dataProvider,
                'resultOrder' => $dataProviderOrder
            ]
        );
    }

    public function actionSearchAuto()
    {
        $qq = Yii::$app->request->get('term');
        $query = Customer::find()->select(['phone as value'])->where(['like','phone', $qq])
            ->asArray('title')
            ->all();
        $keyword = Keywords::find()->select(['key as value'])->where(['like','key', $qq])
            ->asArray('title')
            ->all();
        $result = array();
        foreach($query as $q){
            if (strstr($q['value'], $qq)) {
                $result[] = array('value' => $q['value']);
            }
        }
        $query = Customer::find()->select(['full_name as value'])->where(['like','full_name', $qq])
            ->asArray('title')
            ->all();
        foreach($query as $q){
            if (strstr($q['value'], $qq)) {
                $result[] = array('value' => $q['value']);
            }
        }
        foreach($keyword as $k){
            if (strstr($k['value'], $qq)) {
                $result[] = array('value' => $k['value']);
            }
        }
        return json_encode($result);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $query = AdminNews::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
