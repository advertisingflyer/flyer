<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the dropping of table `address_column_of_customer`.
 */
class m180621_183115_drop_address_column_of_customer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('customer','address');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('customer','address',Schema::TYPE_STRING.'(512)');
    }
}
