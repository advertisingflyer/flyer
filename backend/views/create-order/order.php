<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15.06.2018
 * Time: 2:08
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use backend\models\Paper;
use backend\models\TypePrint;
use backend\models\TypeOrder;
use kartik\datetime\DateTimePicker;

/* @var $this \yii\web\view */
/* @var $model \backend\models\Order */
/* @var $form  \yii\widgets\ActiveForm */
/* @var $id int */

$this->title = 'Create Order';

$model->customer_id = $id;
$this->registerJsFile(Yii::getAlias('@web') . '/js/get-price.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>
<div class="input-block">

    <h3>Заказ на имя: <?= $model->customer->full_name ?></h3>

    <?php $form = ActiveForm::begin(); ?>

    <?= Html::hiddenInput('customer_id', $id) ?>

    <?= $form->field($model, 'type')->dropDownList(ArrayHelper::map(TypeOrder::find()->all(), 'id', 'type'),[
        'prompt' => 'Тип заказа',
        'class' => 'form-control order-type'
    ])->label(false) ?>

    <?= $form->field($model, 'service')->radioList([
        '0' => 'без дизайна',
        '1' => 'с дизайном',
    ], ['class' => 'order-service'])->label(false) ?>

    <?= $form->field($model, 'paper')->dropDownList(ArrayHelper::map(Paper::find()->all(), 'id', 'type'),[
        'prompt' => 'Бумага',
        'class' => 'form-control order-paper'
    ])->label(false) ?>

    <?= $form->field($model, 'type_print')->dropDownList(ArrayHelper::map(TypePrint::find()->all(), 'id', 'type'),[
        'prompt' => 'Тип печати',
        'class' => 'form-control order-type-print'
    ])->label(false) ?>

    <?= $form->field($model, 'sides')->dropDownList([
        '1' => 'односторонняя печать',
        '2' => 'двухсторонняя печать',
    ],['class' => 'form-control order-sides'])->label(false) ?>

    <?= $form->field($model, 'edition')->textInput(['class' => 'form-control order-edition']) ?>

    <div class="price-order-block">
        <div class="price-order-input">
            <?= $form->field($model, 'price')->textInput(['maxlength' => true, 'class' => 'form-control order-price']) ?>
        </div>
        <div class="price-order-input-btn">
            <button type="button" class="price-order-btn btn btn-success"><span class="glyphicon glyphicon-retweet"></span></button>
        </div>
    </div>

    <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([
        '0' => 'Простой заказ',
        '1' => 'Важный заказ',
        '2' => 'Срочный заказ',
    ]) ?>

    <?= $form->field($model, 'deadline')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Enter event time ...','class' => 'form-control modal-sm',],
        'name' => 'dp_4',
        'language' => 'ru-RU',
        'type' => DateTimePicker::TYPE_INLINE,
//        'value' => 'Tue, 23-Feb-1982, 14:45',
        'pluginOptions' => [
            'format' => 'D, dd-M-yyyy, hh:ii'
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
