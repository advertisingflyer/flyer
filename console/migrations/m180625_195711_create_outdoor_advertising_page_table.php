<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `outdoor_advertising_page`.
 */
class m180625_195711_create_outdoor_advertising_page_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('outdoor_advertising_page', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING,
            'description' => Schema::TYPE_STRING.'(16384)',
            'image' => Schema::TYPE_STRING,
            'created_at' => Schema::TYPE_STRING.' NOT NULL',
            'updated_at' => Schema::TYPE_STRING,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('outdoor_advertising_page');
    }
}
