<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180621_184424_add_column_image_in_carousel_table
 */
class m180621_184424_add_column_image_in_carousel_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('carousel','image', Schema::TYPE_STRING);
    }

    public function down()
    {
       $this->dropColumn('carousel','image');
    }
}
