<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TypePrint */

$this->title = 'Update Type Print: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Type Prints', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="type-print-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
