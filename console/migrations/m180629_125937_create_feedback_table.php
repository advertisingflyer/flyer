<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `feedback`.
 */
class m180629_125937_create_feedback_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('feedback', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING.' NOT NULL',
            'state' => Schema::TYPE_SMALLINT,
            'email' => Schema::TYPE_STRING,
            'phone' => Schema::TYPE_STRING,
            'description' => Schema::TYPE_STRING.'(16384) ',
            'created_at' => Schema::TYPE_STRING,
            'answer' => Schema::TYPE_STRING
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('feedback');
    }
}
