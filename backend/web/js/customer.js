$('.customer-btn-search').on('click', function () {

    let phone = $('.customer-phone').val();
    $.ajax(
        {
            url : '/create-order/customer-history',
            type: "POST",
            data : {customer: phone},
            beforeSend: function() {
                $('.wrapper').show();
            },
            complete: function() {
                $('.wrapper').hide();
            },
            success: function(res) {
                $('.customer-order').html(res);
            },
            error: function(res) {
                console.log(res);
            }
        }
    );
});