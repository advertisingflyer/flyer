<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `type_print`.
 */
class m180613_184910_create_type_print_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('type_print', [
            'id' => Schema::TYPE_PK,
            'type' => Schema::TYPE_STRING.' NOT NULL',
            'price' => Schema::TYPE_MONEY
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('type_print');
    }
}
