var sideBlock = '';
$('.menu-a').on('click', function () {
    if (sideBlock != ''){
        $('.' + sideBlock).hide();
    }
    if (sideBlock != $(this).data('id'))
    {
        sideBlock = $(this).data('id');
        $('.' + sideBlock).show();
    } else {
        sideBlock = '';
    }
});

$('.menu-user').on('click', function () {
    $('.dropdown-content').toggle();
});

$(document).mouseup(function (e) {
    let container = $(".dropdown-content");
    if (container.has(e.target).length === 0){
        container.hide();
    }
});