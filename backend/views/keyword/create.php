<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 07.09.2018
 * Time: 18:55
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $model \backend\models\Keywords */
/* @var $form \yii\widgets\ActiveForm */

$this->title = 'Create keyword';

?>
<div class="create-keyword">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'key')->textInput(); ?>
    <?= $form->field($model, 'table')->textInput(); ?>
    <?= $form->field($model, 'column')->textInput(); ?>
    <?= $form->field($model, 'state')->textInput(); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

