<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15.06.2018
 * Time: 15:55
 */

use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this \yii\web\view */
/* @var $model \backend\models\Order */
/* @var $form  \yii\widgets\ActiveForm */

?>
<div class="order-data-history">
    <div class="order-history-info">
        <table>
            <tr>
                <td><b>Номер заказа:</b></td>
                <td><?= $model->id ?></td>
            </tr>
            <tr>
                <td><b>Статус заказа:</b></td>
                <td>
                    <?php if ($model->status == 0): ?>
                        Простой заказ
                    <?php elseif ($model->status == 1): ?>
                        Важный заказ
                    <?php elseif ($model->status == 2): ?>
                        Срочный заказ
                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td><b>Свойства заказа:</b></td>
                <td>
                    <span class="badge"><?= $model->typeOrder->type ?></span>
                    <span class="badge"><?= $model->typePaper->type ?></span>
                    <span class="badge"><?= $model->typePrint->type ?></span>
                    <?php if ($model->sides == 1): ?>
                        <span class="badge">односторонняя печать</span>
                    <?php else: ?>
                        <span class="badge">двухсторонняя печать</span>
                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td><b>Дополнительные услуги:</b></td>
                <td>
                    <?php if ($model->service == 0): ?>
                        без дизайна
                    <?php else: ?>
                        с дизайном
                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td><b>Тираж:</b></td>
                <td><?= $model->edition ?></td>
            </tr>
            <tr>
                <td><b>Цена:</b></td>
                <td><?= $model->price ?></td>
            </tr>
            <tr>
                <td><b>Выплачено</b></td>
                <td><?= $model->paid_out ?></td>
            </tr>
            <tr>
                <td><b>Создан</b></td>
                <td><?= $model->created_at ?></td>
            </tr>
            <?php if ($model->state == 2): ?>
                <p><span class="badge badge-order-warning">Приостановлен: <b>Deadline:</b> <?= $model->deadline ?></span></p>
            <?php else: ?>
                <tr>
                    <td><b>Deadline:</b></td>
                    <td><?= $model->deadline ?></td>
                </tr>
                <?php if ($model->state == 3): ?>
                    <p><span class="badge badge-order-canceled"><b>Отменен:</b> <?= $model->completed ?></span></p>
                <?php else: ?>
                    <?php if ($model->completed != null): ?>
                        <p><span class="badge badge-order-success"><b>Выполнен:</b> <?= $model->completed ?></span></p>
                    <?php else: ?>
                        <p><span class="badge badge-order-wait"><b>Выполняется</b></span></p>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endif; ?>
        </table>
    </div>
    <div class="order-history-control">
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span>',['/create-order/order-success', 'id' => $model->id], ['class' => 'btn btn-control', 'data-pjax' => false]) ?>
        <?php if ($model->completed == null): ?>
            <?php
            Modal::begin([
                'toggleButton' => [
                    'label' => '<span class="glyphicon glyphicon-cog"></span>',
                    'tag' => 'button',
                    'data-id' => $model->id,
                    'class' => 'btn btn-control edit-order-btn',
                    'id' => 'btn-edit-order'
                ],
                'id' => 'edit-order',//.$model->id,
                'header' => '<h2>Редактирование заказа</h2>'

            ]);
            ?>
            <div class="wrapper" id="#loading">
                <div class="inner">
                    <span>З</span>
                    <span>а</span>
                    <span>г</span>
                    <span>р</span>
                    <span>у</span>
                    <span>з</span>
                    <span>к</span>
                    <span>а</span>
                </div>
            </div>
            <?php Modal::end(); ?>
        <?php endif; ?>
        <?php
        Modal::begin([
            'toggleButton' => [
                'label' => '<span class="glyphicon glyphicon-copy"></span>',
                'tag' => 'button',
                'data-id' => $model->id,
                'class' => 'btn btn-control copy-order-btn',
                'id' => 'btn-copy-order'
            ],
            'id' => 'copy-order',//.$model->id,
            'header' => '<h2>Клонирование заказа</h2>'

        ]);
        ?>
        <div id="loading">
            <div class="wrapper">
                <div class="inner">
                    <span>З</span>
                    <span>а</span>
                    <span>г</span>
                    <span>р</span>
                    <span>у</span>
                    <span>з</span>
                    <span>к</span>
                    <span>а</span>
                </div>
            </div>
        </div>
        <?php Modal::end(); ?>
        <?= Html::a('<span class="glyphicon glyphicon-print"></span>',['/create-order/print-order', 'id' => $model->id], ['class' => 'btn btn-control btn-print']) ?>
        <?php if ($model->completed == null): ?>
            <?php
            Modal::begin([
                'toggleButton' => [
                    'label' => '<span class="glyphicon glyphicon-off"></span>',
                    'tag' => 'button',
                    'data-id' => $model->id,
                    'class' => 'btn btn-control off-order-btn',
                    'id' => 'off-order-btn'
                ],
                'id' => 'off-order',//.$model->id,
                'header' => '<h3>Управление</h3>'

            ]);
            ?>
            <div id="loading">
                <div class="wrapper">
                    <div class="inner">
                        <span>З</span>
                        <span>а</span>
                        <span>г</span>
                        <span>р</span>
                        <span>у</span>
                        <span>з</span>
                        <span>к</span>
                        <span>а</span>
                    </div>
                </div>
            </div>
            <?php Modal::end(); ?>
        <?php endif; ?>
        <?php if ($model->completed == null): ?>
            <?php
            Modal::begin([
                'toggleButton' => [
                    'label' => '<span class="glyphicon glyphicon-ok"></span>',
                    'tag' => 'button',
                    'data-id' => $model->id,
                    'class' => 'btn btn-control btn-order-completed',
                    'id' => 'btn-order-completed'
                ],
                'id' => 'completed-order',//.$model->id,
                'header' => '<h2>Выполнение заказа</h2>'

            ]);
            ?>
            <div id="loading">
                <div class="wrapper">
                    <div class="inner">
                        <span>З</span>
                        <span>а</span>
                        <span>г</span>
                        <span>р</span>
                        <span>у</span>
                        <span>з</span>
                        <span>к</span>
                        <span>а</span>
                    </div>
                </div>
            </div>
            <?php Modal::end(); ?>
        <?php endif; ?>
    </div>
</div>
