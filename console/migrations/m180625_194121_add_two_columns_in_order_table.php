<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180625_194121_add_two_columns_in_order_table
 */
class m180625_194121_add_two_columns_in_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order','state', Schema::TYPE_SMALLINT.' DEFAULT 0');
        $this->addColumn('order','updated_at', Schema::TYPE_STRING);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order', 'updated_at');
        $this->dropColumn('order','state');
    }
}
