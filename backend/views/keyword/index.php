<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 07.09.2018
 * Time: 18:53
 */

use yii\bootstrap\Html;
use yii\widgets\ListView;

/* @var $this \yii\web\View */

$this->title = 'Keyword';

?>
<?= Html::a('Добавить', ['/keyword/create'], ['class' => 'btn btn-save']); ?>

<div class="search-list">
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'summary' => '',
        'options' => ['data-pjax'=> true],
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model) {
            return $this->renderAjax('_data',[
                'model' => $model
            ]);
        },
    ]) ?>
</div>
