<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "feedback_phone".
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $created_at
 * @property string $answer
 */
class FeedbackPhone extends \yii\db\ActiveRecord
{
    /**
     * Phone request success constant
     */
    const PHONE_SUCCESS = 'Ваш звонок принят, мы перезвоним :)';

    /**
     * Phone request error constant
     */
    const PHONE_ERROR = 'Трунь, произошла ошибка';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedback_phone';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['name', 'phone', 'created_at', 'answer'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'phone' => 'Phone',
            'created_at' => 'Created At',
            'answer' => 'Answer',
        ];
    }
}
