<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Handles the creation of table `feedback_phone`.
 */
class m180818_203215_create_feedback_phone_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('feedback_phone', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'phone' => Schema::TYPE_STRING . ' NOT NULL',
            'created_at' => Schema::TYPE_STRING,
            'answer' => Schema::TYPE_STRING
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('feedback_phone');
    }
}
