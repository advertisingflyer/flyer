<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "feedback_question".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $question
 * @property string $created_at
 * @property string $answer
 */
class FeedbackQuestion extends \yii\db\ActiveRecord
{
    /**
     * Question request success constant
     */
    const QUESTION_SUCCESS = 'Ваш вопрос добавлен к рассмотрению! :)';

    /**
     * Question request error constant
     */
    const QUESTION_ERROR = 'Ошибка вопроса';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedback_question';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'question'], 'required'],
            [['name', 'email', 'created_at', 'answer'], 'string', 'max' => 255],
            [['question'], 'string', 'max' => 8096],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'question' => 'Question',
            'created_at' => 'Created At',
            'answer' => 'Answer',
        ];
    }
}
