<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `admin_news`.
 */
class m180626_172115_create_admin_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('admin_news', [
            'id' => Schema::TYPE_PK,
            'author' => Schema::TYPE_INTEGER,
            'title' => Schema::TYPE_STRING,
            'description' => Schema::TYPE_STRING.'(16384) NOT NULL',
            'image' => Schema::TYPE_STRING,
            'created_at' => Schema::TYPE_STRING,
            'updated_at' => Schema::TYPE_STRING
        ]);

        $this->addForeignKey(
            'key_admin_news_user',
            'admin_news',
            'author',
            'user',
            'id',
            'set null',
            'cascade'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('key_admin_news_user', 'admin_news');
        $this->dropTable('admin_news');
    }
}
