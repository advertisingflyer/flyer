<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Carousel;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\CarouselSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Carousels';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= Html::a('Добавить фотографию', ['/carousel/create'], ['class' => 'btn btn-create']); ?>

<div class="carousel-index">
    <?= Carousel::widget([
    'items' => $items,
    'options' => ['class' => 'carousel slide', 'data-interval' => '5000'],
    'controls' => [
        '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>',
        '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>'
    ]
    ]); ?>
</div>
