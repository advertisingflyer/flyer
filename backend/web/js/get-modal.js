$(document).ajaxSuccess(function() {

    $('.btn-print').on('click', function (evt) {
        evt.preventDefault();
        window.open(evt.target.href);
    });

    $('.copy-order-btn').on('click', function () {
        let orderID = $(this).data('id');
        let type = 'clone';
        console.log(orderID);
        $.ajax(
            {
                url : '/create-order/get-modal',
                type: "POST",
                data : {order: orderID, type: type},
                beforeSend: function() {
                    $('.wrapper').show();
                },
                complete: function() {
                    $('.wrapper').hide();
                },
                success: function(res) {
                    $('#copy-order .modal-body').html(res);
                    $('.price-order-btn').on('click', getPrice);
                },
                error: function(res) {
                    console.log(res);
                }
            }
        );
    });
    $('.edit-order-btn').on('click', function () {
        let orderID = $(this).data('id');
        let type = 'edit';
        console.log(orderID);
        $.ajax(
            {
                url : '/create-order/get-modal',
                type: "POST",
                data : {order: orderID, type: type},
                beforeSend: function() {
                    $('.wrapper').show();
                },
                complete: function() {
                    $('.wrapper').hide();
                },
                success: function(res) {
                    $('#edit-order .modal-body').html(res);
                    $('.price-order-btn').on('click', getPrice);
                },
                error: function(res) {
                    console.log(res);
                }
            }
        );
    });

    $('.btn-order-completed').on('click', function () {
        let orderID = $(this).data('id');
        $.ajax(
            {
                url : '/create-order/pay-modal',
                type: "POST",
                data : {order: orderID},
                beforeSend: function() {
                    $('.wrapper').show();
                },
                complete: function() {
                    $('.wrapper').hide();
                },
                success: function(res) {
                    $('#completed-order .modal-body').html(res);
                    $('.btn-set-paid').on('click', pay);
                    $('.btn-close-order').on('click', closeOrder);
                },
                error: function(res) {
                    console.log(res);
                }
            }
        );
    });

    $('.off-order-btn').on('click', function () {
        let orderID = $(this).data('id');
        $.ajax(
            {
                url : '/create-order/control-modal',
                type: "POST",
                data : {order: orderID},
                beforeSend: function() {
                    $('.wrapper').show();
                },
                complete: function() {
                    $('.wrapper').hide();
                },
                success: function(res) {
                    $('#off-order .modal-body').html(res);
                    $('.pause-order-btn').on('click', pauseOrder);
                    $('.canceled-order-btn').on('click', canceledOrder);
                },
                error: function(res) {
                    console.log(res);
                }
            }
        );
    });

    $('#copy-order').on('hidden.bs.modal', function () {
        $('#copy-order #display-modal').remove();
    });
    $('#edit-order').on('hidden.bs.modal', function () {
        $('#edit-order #display-modal').remove();
    });
    $('#completed-order').on('hidden.bs.modal', function () {
        $('#completed-order .display-modal').remove();
    });

    function getPrice() {
        var type = $('.order-type').val();
        var paper = $('.order-paper').val();
        var typePrint = $('.order-type-print').val();
        var service = $('.order-service').val();
        var edition = $('.order-edition').val();
        var sides = $('.order-sides').val();
        $.ajax(
            {
                url : '/create-order/get-price',
                type: "POST",
                data : {type: type, paper: paper, typePrint: typePrint, edition: edition, sides: sides, service: service},
                success: function(res) {
                    $('.order-price').val(res);
                },
                error: function(res) {
                    console.log(res);
                }
            }
        );
    }

    function pay() {
        let amount = $('.order-amount').val();
        let orderID = $('.btn-set-paid').data('id');
        $.ajax(
            {
                url : '/create-order/set-paid',
                type: "POST",
                data : {amount: amount, order: orderID},
                beforeSend: function() {
                    $('#completed-order .display-modal').remove();
                    $('.wrapper').show();
                },
                complete: function() {
                    $('.wrapper').hide();
                },
                success: function(res) {
                    $('#completed-order .modal-body').html(res);
                    $('.btn-set-paid').on('click', pay);
                    $('.btn-close-order').on('click', closeOrder);
                },
                error: function(res) {
                    console.log(res);
                }
            }
        );
    }

    function closeOrder() {
        let orderID = $('.btn-close-order').data('id');
        $.ajax(
            {
                url : '/create-order/close-order',
                type: "POST",
                data : {order: orderID},
                beforeSend: function() {
                    $('#completed-order .display-modal').remove();
                    $('.wrapper').show();
                },
                complete: function() {
                    $('.wrapper').hide();
                },
                error: function(res) {
                    console.log(res);
                }
            }
        );
    }

    function pauseOrder() {
        let orderID = $('.pause-order-btn').data('id');
        $.ajax(
            {
                url : '/create-order/suspend-order',
                type: "POST",
                data : {order: orderID},
                beforeSend: function() {
                    $('#off-order .display-modal').remove();
                    $('.wrapper').show();
                },
                complete: function() {
                    $('.wrapper').hide();
                },
                error: function(res) {
                    console.log(res);
                }
            }
        );
    }

    function canceledOrder() {
        let orderID = $('.canceled-order-btn').data('id');
        $.ajax(
            {
                url : '/create-order/canceled-order',
                type: "POST",
                data : {order: orderID},
                beforeSend: function() {
                    $('#off-order .display-modal').remove();
                    $('.wrapper').show();
                },
                complete: function() {
                    $('.wrapper').hide();
                },
                error: function(res) {
                    console.log(res);
                }
            }
        );
    }
});