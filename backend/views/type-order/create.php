<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TypeOrder */

$this->title = 'Create Type Order';
$this->params['breadcrumbs'][] = ['label' => 'Type Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="type-order-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
