<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180616_180636_alter_column_type_in_order_table
 */
class m180616_180636_alter_column_type_in_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('order','type',Schema::TYPE_INTEGER.' NOT NULL');
        $this->addForeignKey(
            'key_order_type',
            'order',
            'type',
            'type_order',
            'id',
            'no action','
            cascade'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('key_order_type','order');
        $this->alterColumn('order','type',Schema::TYPE_STRING.' NOT NULL');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180616_180636_alter_column_type_in_order_table cannot be reverted.\n";

        return false;
    }
    */
}
