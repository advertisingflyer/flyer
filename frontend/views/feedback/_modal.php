<?php
/**
 * Created by PhpStorm.
 * User: nikita
 * Date: 14.08.2018
 * Time: 21:20
 * @var $this \yii\web\View
 * @var $phone \common\models\FeedbackPhone
 * @var $question \common\models\FeedbackQuestion
 */
use \yii\widgets\ActiveForm;
use \yii\widgets\MaskedInput;
?>
<div class="display-modal" id="display-modal">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#phone">Звонилка</a></li>
        <li><a data-toggle="tab" href="#question">Спрашивалка</a></li>
    </ul>
    <div class="tab-content">
        <div id="phone" class="tab-pane fade in active">
            <? $phoneForm = ActiveForm::begin([
                    'id' => 'phone-form'
            ]); ?>
            <?= $phoneForm->field($phone, 'name');?>
            <?= $phoneForm->field($phone, 'phone')->widget(MaskedInput::className(), [
                    'mask' => [ '+9{1,3}(9{1,5})9{7,8}'], // Великобритания вроде на 1 цифру больше
                    'clientOptions'=>[
                        'clearIncomplete'=>true
                    ]
            ]);?>
            <?= \yii\helpers\Html::submitButton('Отправить', [
                'class' => 'btn btn-success'
            ]); ?>
            <? $phoneForm = ActiveForm::end(); ?>
        </div>
        <div id="question" class="tab-pane fade">
            <? $questionForm = ActiveForm::begin([
                    'id' => 'question-form',
            ]); ?>
            <?= $phoneForm->field($question, 'name');?>
            <?= $phoneForm->field($question, 'email')->widget(MaskedInput::className(), [
                'clientOptions' => [
                    'alias' =>  'email',
                    'clearIncomplete'=>true
                ],
            ]);?>
            <?= $phoneForm->field($question, 'question');?>
            <?= \yii\helpers\Html::submitButton('Отправить', [
                'class' => 'btn btn-success'
            ]); ?>
            <? $questionForm = ActiveForm::end(); ?>
        </div>
    </div>
</div>
