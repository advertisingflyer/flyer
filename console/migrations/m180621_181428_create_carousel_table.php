<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `carousel`.
 */
class m180621_181428_create_carousel_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('carousel', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING.'',
            'description' => Schema::TYPE_STRING.'(1024)',
            'created_at' => Schema::TYPE_STRING.' NOT NULL',
            'updated_at' => Schema::TYPE_STRING,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('carousel');
    }
}
