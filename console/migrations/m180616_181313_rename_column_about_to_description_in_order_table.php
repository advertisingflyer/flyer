<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180616_181313_rename_column_about_to_description_in_order_table
 */
class m180616_181313_rename_column_about_to_description_in_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('order','about','description');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('order','description','about');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180616_181313_rename_column_about_to_description_in_order_table cannot be reverted.\n";

        return false;
    }
    */
}
