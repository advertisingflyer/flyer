<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180707_115617_add_column_paid_out_in_order_table
 */
class m180707_115617_add_column_paid_out_in_order_table extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('order', 'paid_out', Schema::TYPE_MONEY);
    }

    public function down()
    {
        $this->dropColumn('order','paid_out');
    }

}
