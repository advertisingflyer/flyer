<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 07.09.2018
 * Time: 18:51
 */

namespace backend\controllers;


use backend\models\Keywords;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class KeywordController extends Controller
{
    public function actionIndex()
    {
        $query = Keywords::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionCreate()
    {
        $model =  new Keywords();

        return $this->render('create',
            [
                'model' => $model
            ]
        );
    }
}