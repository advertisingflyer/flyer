<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `reviews`.
 */
class m180613_180231_create_reviews_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('reviews', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER,
            'username' => Schema::TYPE_STRING.' NOT NULL',
            'email' => Schema::TYPE_STRING.' NOT NULL',
            'phone' => Schema::TYPE_INTEGER.' NOT NULL',
            'text' => Schema::TYPE_STRING.'(8196) NOT NULL',
            'created_at' => Schema::TYPE_DATETIME.' NOT NULL'
        ]);

        $this->addForeignKey(
            'key_reviews_user',
            'reviews',
            'user_id',
            'user',
            'id',
            'set null',
            'cascade'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('key_reviews_user','reviews');
        $this->dropTable('reviews');
    }
}
