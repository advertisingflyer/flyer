<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `feedback`.
 */
class m180818_203025_drop_feedback_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('feedback');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('feedback', [
            'id' => $this->primaryKey(),
        ]);
    }
}
