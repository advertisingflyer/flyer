<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `keywords`.
 */
class m180906_204159_create_keywords_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('keywords', [
            'id' => Schema::TYPE_PK,
            'key' => Schema::TYPE_STRING.' NOT NULL',
            'table' => Schema::TYPE_STRING.'(20) NOT NULL',
            'column' => Schema::TYPE_STRING.'(20) NOT NULL',
            'state' => Schema::TYPE_SMALLINT.' NOT NULL'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('keywords');
    }
}
