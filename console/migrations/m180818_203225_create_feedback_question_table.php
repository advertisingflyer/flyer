<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Handles the creation of table `feedback_question`.
 */
class m180818_203225_create_feedback_question_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('feedback_question', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'question' => Schema::TYPE_STRING . '(8096) NOT NULL',
            'created_at' => Schema::TYPE_STRING,
            'answer' => Schema::TYPE_STRING
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('feedback_question');
    }
}
