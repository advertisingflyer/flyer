<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 27.06.2018
 * Time: 14:34
 */

/* @var $this \yii\web\view */
/* @var $model \backend\models\AdminNews */

?>
<div class="news-data-block">
    <div class="news-head">
        <p>Author: <?= $model->user->username ?></p>
    </div>
    <div class="news-body">
        <p><?= $model->description ?></p>
            <?php $images = $model->getImages(); ?>
            <?php foreach($images as $img): ?>
                <?php if ($img['id'] != null): ?>
                    <img src="<?= $img->getUrl('x200'); ?>" alt="">
                <?php endif; ?>
            <?php endforeach; ?>
    </div>
    <div class="news-footer">
        <p>Дата добавления: <?= $model->created_at ?></p>
        <?php if ($model->updated_at != null): ?>
            <p>Дата обновления: <?= $model->updated_at ?></p>
        <?php endif; ?>
    </div>
</div>
