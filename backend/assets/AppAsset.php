<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/nav.css',
        'css/sidebar.css',
        'css/create_order.css',
        'css/backend.css',
        'css/loading.css',
        'css/customer.css',
        'css/carousel.css',
        'css/search.css'
    ];
    public $js = [
        'js/sidebar.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
