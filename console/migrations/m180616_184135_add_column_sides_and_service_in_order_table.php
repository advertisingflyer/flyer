<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180616_184135_add_column_sides_and_service_in_order_table
 */
class m180616_184135_add_column_sides_and_service_in_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order', 'sides', Schema::TYPE_SMALLINT.' DEFAULT 1');
        $this->addColumn('order','service',Schema::TYPE_SMALLINT.' DEFAULT 1');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order','sides');
        $this->dropColumn('order','service');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180616_184135_add_column_sides_and_service_in_order_table cannot be reverted.\n";

        return false;
    }
    */
}
