<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\WebPage */

$this->title = 'Update Web Page: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Web Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="web-page-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
